package net.suncrescent.clicker;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import net.suncrescent.clicker.robot.action.Action;
import net.suncrescent.clicker.robot.action.ActionSource;
import net.suncrescent.clicker.robot.action.Condition;
import net.suncrescent.clicker.robot.action.Condition.ConditionType;
import net.suncrescent.clicker.robot.action.MouseAction;
import net.suncrescent.clicker.robot.action.MouseAction.MouseActions;

public class FarmVilleActionSource implements ActionSource {

	private ArrayList<Action> actionList = new ArrayList<Action>();

	public FarmVilleActionSource() {

		// if 845,700 is blue
		Condition oneButtonDialogIsVisible = new Condition(ConditionType.PIXEL_COLOR_MATCHES);
		oneButtonDialogIsVisible.setConditionParameters(new Point(845, 700), new Color(2, 114, 171));

		// if 899,699 is blue
		Condition dialogIsVisible = new Condition(ConditionType.PIXEL_COLOR_MATCHES);
		dialogIsVisible.setConditionParameters(new Point(899, 699), new Color(2, 114, 171));

		// Setup Time
		this.actionList.add(new MouseAction(15000, 1216, 572, MouseActions.NONE));

		// Dialog
		this.actionList.add(new MouseAction(600, 935, 703, MouseActions.NONE, oneButtonDialogIsVisible));
		this.actionList.add(new MouseAction(100, 935, 703, MouseActions.LEFT_CLICK, oneButtonDialogIsVisible));
		// Dialog
		this.actionList.add(new MouseAction(600, 858, 703, MouseActions.NONE, dialogIsVisible));
		this.actionList.add(new MouseAction(100, 858, 703, MouseActions.LEFT_CLICK, dialogIsVisible));
		// Northwest Stadium
		this.actionList.add(new MouseAction(600, 830, 725, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 820, 725, MouseActions.LEFT_CLICK));
		// Mall
		this.actionList.add(new MouseAction(600, 1339, 583, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1339, 583, MouseActions.LEFT_CLICK));
		// North Hospital
		this.actionList.add(new MouseAction(600, 1552, 586, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1552, 586, MouseActions.LEFT_CLICK));
		// Petshop
		this.actionList.add(new MouseAction(600, 1709, 903, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1709, 903, MouseActions.LEFT_CLICK));
		// Drag Map
		this.actionList.add(new MouseAction(600, 1489, 891, MouseActions.NONE));
		this.actionList.add(new MouseAction(300, 1489, 891, MouseActions.LEFT_DOWN));
		this.actionList.add(new MouseAction(300, 869, 62, MouseActions.LEFT_UP));
		// West Casino
		this.actionList.add(new MouseAction(600, 712, 202, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 712, 202, MouseActions.LEFT_CLICK));
		// Flowers
		this.actionList.add(new MouseAction(600, 705, 493, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 705, 493, MouseActions.LEFT_CLICK));
		// Stadium
		this.actionList.add(new MouseAction(600, 1105, 354, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1105, 354, MouseActions.LEFT_CLICK));
		// East Casino
		this.actionList.add(new MouseAction(600, 1298, 197, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1298, 197, MouseActions.LEFT_CLICK));
		// East Hospital
		this.actionList.add(new MouseAction(600, 1575, 336, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1575, 336, MouseActions.LEFT_CLICK));
		// South East Stadium
		this.actionList.add(new MouseAction(600, 1575, 636, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1575, 636, MouseActions.LEFT_CLICK));
		// Bowling
		this.actionList.add(new MouseAction(600, 592, 705, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 592, 705, MouseActions.LEFT_CLICK));
		// Disco
		this.actionList.add(new MouseAction(600, 1140, 739, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 1140, 739, MouseActions.LEFT_CLICK));
		// South West Hospital
		this.actionList.add(new MouseAction(600, 157, 324, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 157, 324, MouseActions.LEFT_CLICK));
		// North West Hospital
		this.actionList.add(new MouseAction(600, 157, 202, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 157, 202, MouseActions.LEFT_CLICK));
		// South West Stadium
		this.actionList.add(new MouseAction(600, 170, 705, MouseActions.NONE));
		this.actionList.add(new MouseAction(100, 170, 705, MouseActions.LEFT_CLICK));
		// Drag Map
		this.actionList.add(new MouseAction(600, 393, 384, MouseActions.NONE));
		this.actionList.add(new MouseAction(300, 393, 384, MouseActions.LEFT_DOWN));
		this.actionList.add(new MouseAction(100, 715, 915, MouseActions.LEFT_UP));
		this.actionList.add(new MouseAction(600, 393, 384, MouseActions.NONE));
		this.actionList.add(new MouseAction(300, 393, 384, MouseActions.LEFT_DOWN));
		this.actionList.add(new MouseAction(150000, 693, 684, MouseActions.LEFT_UP));
	}

	public ArrayList<Action> getActionList() {
		return this.actionList;
	}
}
