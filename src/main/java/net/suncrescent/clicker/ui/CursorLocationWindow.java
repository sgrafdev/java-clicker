package net.suncrescent.clicker.ui;

import java.awt.MouseInfo;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JWindow;

import org.apache.log4j.Logger;

public class CursorLocationWindow implements Runnable {

	private final static Logger log = Logger.getLogger(CursorLocationWindow.class);

	private final JLabel cursorLabel = new JLabel();
	private JWindow cursorWindow = new JWindow();

	private volatile boolean isRunning = true;

	public CursorLocationWindow() {

		CursorLocationWindow.log.debug("Constructor");

		this.setup();

		CursorLocationWindow.log.debug("Starting thread");

		final Thread runnerThread = new Thread(this);
		runnerThread.setDaemon(true);
		runnerThread.start();
	}

	@Override
	public void run() {

		CursorLocationWindow.log.debug("Thread started");

		try {

			while (this.isRunning()) {
				final Point location = MouseInfo.getPointerInfo().getLocation();
				this.moveToTop();
				this.setText(location);
				location.translate(10, 10);
				this.setPosition(location);
				// approx. 27 updates/second
				Thread.sleep(35);
			}

		} catch (final InterruptedException ie) {
			CursorLocationWindow.log.error(ie, ie);
		}

		CursorLocationWindow.log.debug("Disposing resources");

		// dispose window so that all AWT threads can terminate
		this.cursorWindow.setVisible(false);
		this.cursorWindow.dispose();
		this.cursorWindow = null;

		CursorLocationWindow.log.debug("Thread ended");
	}

	private void setup() {
		this.cursorWindow.getContentPane().add(this.cursorLabel);
		this.cursorWindow.setSize(91, 15);
		this.cursorWindow.setAlwaysOnTop(true);
		this.cursorWindow.setVisible(true);
	}

	private void setText(final Point location) {
		this.cursorLabel.setText("x=" + (int) location.getX() + ", y=" + (int) location.getY());
	}

	private void setPosition(final Point location) {
		this.cursorWindow.setLocation(location);
	}

	private void moveToTop() {
		// cheap trick to make sure the label is top-most at all times (will even go over the task manager)
		this.cursorWindow.setAlwaysOnTop(false);
		this.cursorWindow.setAlwaysOnTop(true);
	}

	private synchronized boolean isRunning() {
		return this.isRunning;
	}

	public synchronized void dispose() {
		this.isRunning = false;
	}
}
