package net.suncrescent.clicker;

import java.util.HashMap;
import java.util.Map;

public class Options {

	private final Map<Keys, Boolean> options;

	public Options() {

		this.options = new HashMap<Keys, Boolean>(Keys.values().length);

		// initialize all options with default values
		for (final Keys key : Keys.values()) {
			this.options.put(key, key.getDefaultValue());
		}
	}

	public void setOption(final Keys key, final Boolean value) {
		this.options.put(key, value);
	}

	public Boolean getOption(final Keys key) {
		return this.options.get(key);
	}

	public void readArguments(final String[] args) {
		for (int i = 0; i < args.length; i++) {
			final Keys key = Keys.getKey(args[i]);
			if (key != null) {
				this.options.put(key, Boolean.TRUE);
			}
		}
	}

	@Override
	public String toString() {
		final String newLine = String.format("%n");
		final StringBuilder output = new StringBuilder();
		output.append("Options" + newLine);
		for (final Keys key : Keys.values()) {
			output.append("    " + key.toString() + ": " + this.options.get(key).toString() + newLine);
		}
		return output.toString();
	}

	public enum Keys {
		NO_TOOLTIP("no_tooltip", Boolean.FALSE);

		private final String argValue;
		private final Boolean defaultValue;

		Keys(final String argValue, final Boolean defaultValue) {
			this.argValue = argValue;
			this.defaultValue = defaultValue;
		}

		public Boolean getDefaultValue() {
			return this.defaultValue;
		}

		public String getArgValue() {
			return this.argValue;
		}

		public static Keys getKey(final String argValue) {
			for (final Keys key : Keys.values()) {
				if (key.argValue.equals(argValue)) {
					return key;
				}
			}
			return null;
		}
	}
}
