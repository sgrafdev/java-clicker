package net.suncrescent.clicker;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import net.suncrescent.clicker.robot.action.Action;
import net.suncrescent.clicker.robot.action.ActionSource;
import net.suncrescent.clicker.robot.action.Condition;
import net.suncrescent.clicker.robot.action.Condition.ConditionType;
import net.suncrescent.clicker.robot.action.NullAction;
import net.suncrescent.clicker.robot.action.PlaySoundAction;
import net.suncrescent.clicker.robot.action.PlaySoundAction.SoundFiles;

public class BBActionSource implements ActionSource {

	private ArrayList<Action> actionList = new ArrayList<Action>();

	public BBActionSource() {

		// add to cart button is yellow
		Condition cartButtonYellow = new Condition(ConditionType.PIXEL_COLOR_MATCHES);
		cartButtonYellow.setConditionParameters(new Point(845, 700), new Color(2, 114, 171));

		// Just wait 5 seconds
		this.actionList.add(new NullAction(5000));

		this.actionList.add(new PlaySoundAction(0, SoundFiles.ERROR, cartButtonYellow));

		// Just wait 1 minute if the sound was played to not go crazy
		this.actionList.add(new NullAction(55000, cartButtonYellow));
	}

	public ArrayList<Action> getActionList() {
		return this.actionList;
	}
}
