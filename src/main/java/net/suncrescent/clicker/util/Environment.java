package net.suncrescent.clicker.util;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

public class Environment {

	private static Environment instance;
	private final Dimension screenSize;

	private Environment() {

		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice dev = env.getDefaultScreenDevice();
		DisplayMode displayMode = dev.getDisplayMode();

		this.screenSize = new Dimension(displayMode.getWidth(), displayMode.getHeight());
	}

	public static Dimension getScreenSize() {
		if (instance == null) {
			instance = new Environment();
		}

		// create new copy to prevent any changes
		return new Dimension(instance.screenSize);
	}
}
