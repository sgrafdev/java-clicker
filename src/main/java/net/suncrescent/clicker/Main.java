package net.suncrescent.clicker;

import java.awt.AWTException;

import org.apache.log4j.Logger;

import net.suncrescent.clicker.ui.ControlFrame;

public class Main {

	private final static Logger log = Logger.getLogger(Main.class);

	public static void main(final String[] args) {

		// setup options and use args to override default options
		final Options options = new Options();
		options.readArguments(args);

		Main.log.debug("Application started");

		try {
			final ControlFrame frame = new ControlFrame(options);
			frame.setActionSource(new BBActionSource());
			Main.log.debug("Going into SWT");
			frame.display();
		} catch (final AWTException e) {
			Main.log.error(e, e);
		}
		Main.log.debug("Application terminated");
	}

}