package net.suncrescent.clicker.robot;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import net.suncrescent.clicker.robot.RobotInteractionProvider.RobotControlProvider;
import net.suncrescent.clicker.robot.RobotInteractionProvider.RobotEventProvider;
import net.suncrescent.clicker.robot.RobotInteractionProvider.RobotStatus;
import net.suncrescent.clicker.robot.RobotInteractionProvider.RobotStatusProvider;
import net.suncrescent.clicker.robot.action.Action;
import net.suncrescent.clicker.robot.action.Condition;

public class Robot {

	private final static Logger log = Logger.getLogger(Robot.class);

	private final java.awt.Robot robot;

	private final ArrayList<Action> actionSequence = new ArrayList<Action>();

	private final RobotInteractionProvider interactionProvider;

	private Boolean previousConditionResult = null;
	private int repeatCount = 0;

	public Robot() throws AWTException {
		Robot.log.debug("Robot constructor");
		this.robot = new java.awt.Robot();
		this.robot.setAutoWaitForIdle(true);
		this.interactionProvider = new RobotInteractionProvider(this);
	}

	public RobotStatusProvider getStatusProvider() {
		return this.interactionProvider.getStatusProvider();
	}

	public RobotControlProvider getControlProvider() {
		return this.interactionProvider.getControlProvider();
	}

	public RobotEventProvider getEventProvider() {
		return this.interactionProvider.getEventProvider();
	}

	public void setActionSequence(final List<Action> actions) {
		this.statusCheck();
		this.actionSequence.clear();
		this.actionSequence.addAll(actions);
	}

	public void setRepeatCount(final int count) {
		this.statusCheck();
		this.repeatCount = count;
	}

	protected void executeWork() throws InterruptedException {

		Robot.log.info("Execute work started. Number of actions: " + this.actionSequence.size() + ". Repeat count: " + this.repeatCount);

		// loop
		while (this.hasWorkingStatus() && this.repeatCount > 0) {

			Robot.log.debug("Executing repeat count: " + this.repeatCount);

			for (final Action action : this.actionSequence) {

				Robot.log.debug("Executing action: " + action.toString());

				// check condition
				final Condition cond = action.getCondition();

				if (cond == null || this.evaluateCondition(cond)) {

					action.executeAction(this.robot);

					// pause?
					if (action.getPause() > 0) {
						int pause = action.getPause();
						Robot.log.debug("Pause of " + pause + "ms");
						while (pause > 0 && this.hasWorkingStatus()) {
							Thread.sleep(Math.min(pause, 100));
							pause -= 100;
						}
					}
				}

				if (!this.hasWorkingStatus()) {
					return;
				}

				// lets be polite
				Thread.yield();
			}

			this.interactionProvider.getEventProvider().notifyRepeatCountChange(--this.repeatCount);

			// paused by controller?
			while (this.hasWorkingStatus() && this.interactionProvider.getStatusProvider().getStatus() == RobotStatus.PAUSED) {
				Thread.sleep(100);
			}
		}

		Robot.log.info("Execute work complete");
	}

	private boolean evaluateCondition(final Condition condition) {

		Robot.log.debug("Condition evaluate. Type: " + condition.getConditionType());

		boolean result = false;

		final Object[] params = condition.getConditionParameters();

		// evaluate
		switch (condition.getConditionType()) {
		case PIXEL_COLOR_MATCHES:
			if (params != null && params.length == 2) {
				final Point position = (Point) params[0];
				final Color color = (Color) params[1];
				final Color pixelColor = this.robot.getPixelColor(position.x, position.y);
				Robot.log.debug("Position (" + position.x + ", " + position.y + "). Expected color: " + color.toString() + ". Pixel color: "
						+ pixelColor.toString());
				result = (color.equals(pixelColor));
			}
			break;
		case PIXEL_COLOR_NOT_MATCHES:
			if (params != null && params.length == 2) {
				final Point position = (Point) params[0];
				final Color color = (Color) params[1];
				final Color pixelColor = this.robot.getPixelColor(position.x, position.y);
				Robot.log.debug("Position (" + position.x + ", " + position.y + "). Not expected color: " + color.toString() + ". Pixel color: "
						+ pixelColor.toString());
				result = (!color.equals(pixelColor));
			}
			break;
		case PREVIOUS_CONDITION_TRUE:
			result = (this.previousConditionResult != null && this.previousConditionResult == true);
			break;
		case PREVIOUS_CONDITION_FALSE:
			result = (this.previousConditionResult == null || this.previousConditionResult == false);
			break;
		}

		// check for chained operation
		if (condition.getChainedCondition() != null) {

			switch (condition.getChainedOperator()) {
			default:
			case AND:
				result = (result && this.evaluateCondition(condition.getChainedCondition()));
				break;
			case EQUAL:
				result = (result == this.evaluateCondition(condition.getChainedCondition()));
				break;
			case NOT_EQUAL:
				result = (result != this.evaluateCondition(condition.getChainedCondition()));
				break;
			case OR:
				result = (result || this.evaluateCondition(condition.getChainedCondition()));
				break;
			case XOR:
				result = ((result == true && this.evaluateCondition(condition.getChainedCondition()) == false) || (result == false && this
						.evaluateCondition(condition.getChainedCondition()) == true));
				break;
			}
		}

		this.previousConditionResult = result;

		Robot.log.debug("Condition evaluates as: " + (result ? "true" : "false"));

		return result;
	}

	private boolean hasWorkingStatus() {
		final RobotStatus status = this.interactionProvider.getStatusProvider().getStatus();
		return status == RobotStatus.PAUSED || status == RobotStatus.RUNNING;
	}

	private void statusCheck() throws IllegalStateException {
		if (this.hasWorkingStatus()) {
			throw new IllegalStateException("This method can not be called while the robot is in a running or paused state.");
		}
	}
}
