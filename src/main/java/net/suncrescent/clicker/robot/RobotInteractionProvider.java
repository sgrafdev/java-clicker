package net.suncrescent.clicker.robot;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

public class RobotInteractionProvider {

	private final static Logger log = Logger.getLogger(RobotInteractionProvider.class);

	public enum RobotStatus {
		IDLE, RUNNING, PAUSED, STOPPING
	}

	private final Robot owner;

	private final RobotEventProvider eventProvider;
	private final RobotStatusProvider statusProvider;
	private final RobotControlProvider controlProvider;

	protected RobotInteractionProvider(final Robot owner) {
		this.owner = owner;
		this.eventProvider = new RobotEventProvider();
		this.statusProvider = new RobotStatusProvider();
		this.controlProvider = new RobotControlProvider();
	}

	public RobotStatusProvider getStatusProvider() {
		return this.statusProvider;
	}

	public RobotControlProvider getControlProvider() {
		return this.controlProvider;
	}

	public RobotEventProvider getEventProvider() {
		return this.eventProvider;
	}

	public class RobotStatusProvider {

		private volatile RobotStatus currentStatus = RobotStatus.IDLE;

		public synchronized RobotStatus getStatus() {
			return this.currentStatus;
		}

		private synchronized void setStatus(final RobotStatus newStatus) {
			final RobotStatus oldStatus = this.currentStatus;
			this.currentStatus = newStatus;
			// notify observers
			RobotInteractionProvider.this.eventProvider.notifyStatusChange(oldStatus, newStatus);
		}
	}

	public class RobotControlProvider {

		public void start() {

			RobotInteractionProvider.log.info("start");

			final Thread workThread = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						RobotInteractionProvider.this.statusProvider.setStatus(RobotStatus.RUNNING);
						RobotInteractionProvider.this.owner.executeWork();
						RobotInteractionProvider.this.statusProvider.setStatus(RobotStatus.IDLE);
					} catch (final InterruptedException e) {
						// interrupted because the thread was terminated
					}
				}
			});

			workThread.setDaemon(true);
			workThread.start();
		}

		public boolean stop() {

			RobotInteractionProvider.log.info("stop");

			synchronized (RobotInteractionProvider.this.statusProvider) {
				if (this.getStatus() == RobotStatus.PAUSED || this.getStatus() == RobotStatus.RUNNING) {
					this.setStatus(RobotStatus.STOPPING);
					return true;
				}
			}

			return false;
		}

		public boolean resume() {

			RobotInteractionProvider.log.info("resume");

			synchronized (RobotInteractionProvider.this.statusProvider) {
				if (this.getStatus() == RobotStatus.PAUSED) {
					this.setStatus(RobotStatus.RUNNING);
					return true;
				}
			}

			return false;
		}

		public boolean pause() {

			RobotInteractionProvider.log.info("pause");

			synchronized (RobotInteractionProvider.this.statusProvider) {
				if (this.getStatus() == RobotStatus.RUNNING) {
					this.setStatus(RobotStatus.PAUSED);
					return true;
				}
			}

			return false;
		}

		private RobotStatus getStatus() {
			return RobotInteractionProvider.this.statusProvider.getStatus();
		}

		private void setStatus(final RobotStatus status) {
			RobotInteractionProvider.this.statusProvider.setStatus(status);
		}
	}

	public class RobotEventProvider {

		private final Set<RobotEventListener> listeners = new HashSet<RobotEventListener>();

		private RobotEventProvider() {
		}

		public synchronized boolean addEventListener(final RobotEventListener listener) {
			return this.listeners.add(listener);
		}

		public synchronized boolean removeEventListener(final RobotEventListener listener) {
			return this.listeners.remove(listener);
		}

		protected void notifyStatusChange(final RobotStatus oldStatus, final RobotStatus newStatus) {

			RobotInteractionProvider.log.debug("statusChanged: " + newStatus);

			final Iterator<RobotEventListener> iterator = this.listeners.iterator();
			while (iterator.hasNext()) {
				iterator.next().statusChanged(oldStatus, newStatus);
			}
		}

		protected void notifyRepeatCountChange(final int repeatCount) {

			RobotInteractionProvider.log.debug("repeatcountChanged: " + repeatCount);

			final Iterator<RobotEventListener> iterator = this.listeners.iterator();
			while (iterator.hasNext()) {
				iterator.next().repeatCountChanged(repeatCount);
			}
		}
	}

	public interface RobotEventListener {

		public void statusChanged(RobotStatus oldStatus, RobotStatus newStatus);

		public void repeatCountChanged(int repeatCount);
	}
}