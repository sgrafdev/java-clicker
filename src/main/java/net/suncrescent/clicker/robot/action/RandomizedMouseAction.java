package net.suncrescent.clicker.robot.action;

public class RandomizedMouseAction extends MouseAction {

	private final int rngPause;
	private final int rngX;
	private final int rngY;

	public RandomizedMouseAction(final int rngPause, final int rngX, final int rngY) {
		super();
		this.rngX = rngX;
		this.rngY = rngY;
		this.rngPause = rngPause;
	}

	public RandomizedMouseAction(final int rngPause, final int rngX, final int rngY, final int pause, final int x, final int y, final MouseActions action) {
		super(pause, x, y, action);
		this.rngX = rngX;
		this.rngY = rngY;
		this.rngPause = rngPause;
	}

	public RandomizedMouseAction(final int rngPause, final int rngX, final int rngY, final int pause, final int x, final int y, final MouseActions action,
			final Condition condition) {
		super(pause, x, y, action, condition);
		this.rngX = rngX;
		this.rngY = rngY;
		this.rngPause = rngPause;
	}

	@Override
	public int getPause() {
		return super.getPause() - this.rngPause + (int) (Math.random() * this.rngPause * 2.0);
	}

	@Override
	public int getX() {
		return super.getX() - this.rngX + (int) (Math.random() * this.rngX * 2.0);
	}

	@Override
	public int getY() {
		return super.getY() - this.rngY + (int) (Math.random() * this.rngY * 2.0);
	}

	@Override
	public String toString() {
		return super.getAction().toString() + " at (" + super.getX() + " ±" + this.rngX + ", " + super.getY() + " ±" + this.rngY + ") then wait for "
				+ super.getPause() + " ± " + this.rngPause + "ms.";
	}
}
