package net.suncrescent.clicker.robot.action;

import java.awt.Robot;
import java.io.File;
import java.io.FileInputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class PlaySoundAction extends Action {

  private SoundFiles soundFile;

  public PlaySoundAction() {
    super();
  }

  public PlaySoundAction(int pause, SoundFiles soundFile) {
    super(pause);
    this.setSoundFile(soundFile);
  }

  public PlaySoundAction(int pause, SoundFiles error, Condition condition) {
    super(pause, condition);
    this.setSoundFile(soundFile);
  }

  public SoundFiles getSoundFile() {
    return soundFile;
  }

  public void setSoundFile(SoundFiles soundFile) {
    this.soundFile = soundFile;
  }

  public enum SoundFiles {

    ERROR("res/error.wav");

    private final String fileName;

    SoundFiles(String fileName) {
      this.fileName = fileName;
    }

    public String getFileName() {
      return this.fileName;
    }
  }

  @Override
  public void executeAction(Robot robot) {
    try(FileInputStream fis = new FileInputStream(new File(this.soundFile.fileName))) {
      AudioInputStream stream = AudioSystem.getAudioInputStream(fis);
      AudioFormat format = stream.getFormat();
      DataLine.Info info = new DataLine.Info(Clip.class, format);
      Clip clip = (Clip) AudioSystem.getLine(info);
      clip.open(stream);
      clip.start();
     }
    catch (Exception e) {e.printStackTrace();}
  }
}
