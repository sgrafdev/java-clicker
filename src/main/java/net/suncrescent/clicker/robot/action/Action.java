package net.suncrescent.clicker.robot.action;

import java.awt.Robot;

public abstract class Action {

	private int pause = 0;
	private Condition condition = null;

	public Action() {
	}

	public Action(int pause) {
		this.pause = pause;
	}

	public Action(int pause, Condition condition) {
		this.pause = pause;
		this.condition = condition;
	}

	public int getPause() {
		return this.pause;
	}

	public void setPause(final int pause) {
		this.pause = pause;
	}

	public Condition getCondition() {
		return this.condition;
	}

	public void setCondition(final Condition condition) {
		this.condition = condition;
	}

	public abstract void executeAction(Robot robot);

	@Override
	public String toString() {
		return getClass().getSimpleName().toString();
	}
}
