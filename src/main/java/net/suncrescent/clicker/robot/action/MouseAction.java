package net.suncrescent.clicker.robot.action;

import org.apache.log4j.Logger;
import java.awt.Robot;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class MouseAction extends Action {

	private final static Logger LOGGER = Logger.getLogger(MouseAction.class);

	private int x = -1;
	private int y = -1;
	private MouseActions action;

	public MouseAction() {
	}

	public MouseAction(final int pause, final int x, final int y, final MouseActions action) {
		this(pause, x, y, action, null);
	}

	public MouseAction(final int pause, final int x, final int y, final MouseActions action, final Condition condition) {
		super(pause, condition);
		this.x = x;
		this.y = y;
		this.action = action;
	}

	public int getX() {
		return this.x;
	}

	public void setX(final int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(final int y) {
		this.y = y;
	}

	public MouseActions getAction() {
		return this.action;
	}

	public void setAction(final MouseActions action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return this.action.toString() + " at (" + this.x + ", " + this.y + ") then wait for " + this.getPause() + "ms.";
	}

	public void executeAction(Robot robot) {

		// position the pointer
		this.smoothMove(robot, getX(), getY());

		robot.setAutoDelay(100 + (int) (Math.random() * 50));

		// do button action
		switch (getAction()) {
			case LEFT_DOWN:
				robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
				break;
			case LEFT_UP:
				robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
				break;
			case MIDDLE_DOWN:
				robot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
				break;
			case MIDDLE_UP:
				robot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
				break;
			case RIGHT_DOWN:
				robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
				break;
			case RIGHT_UP:
				robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
				break;
			case LEFT_CLICK:
				robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
				robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
				break;
			case MIDDLE_CLICK:
				robot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
				robot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
				break;
			case RIGHT_CLICK:
				robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
				robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
				break;
			case KEY_B:
				robot.keyPress(KeyEvent.VK_B);
				robot.keyRelease(KeyEvent.VK_B);
				break;
			case KEY_W:
				robot.keyPress(KeyEvent.VK_W);
				robot.keyRelease(KeyEvent.VK_W);
				break;
			case KEY_S:
				robot.keyPress(KeyEvent.VK_S);
				robot.keyRelease(KeyEvent.VK_S);
				break;
			case KEY_X:
				robot.keyPress(KeyEvent.VK_X);
				robot.keyRelease(KeyEvent.VK_X);
				break;
			case NONE:
				// nothing happens!
				break;
		}
	}

	private void smoothMove(Robot robot, final int x, final int y) {

		LOGGER.info("Mouse move to (" + x + ", " + y + ")");

		final PointerInfo pInfo = MouseInfo.getPointerInfo();

		final Point origin = pInfo.getLocation();

		LOGGER.debug("Current pointer position (" + origin.getX() + ", " + origin.getY() + ")");

		int differenceX = (int) (x - origin.getX());
		int differenceY = (int) (y - origin.getY());

		if (x == -1) {
			differenceX = 0;
		}

		if (y == -1) {
			differenceY = 0;
		}

		if (differenceX != 0 || differenceY != 0) {

			final double step = (((int) Math.random() * 2 + 2)
					/ Math.sqrt(Math.pow(differenceX, 2) + Math.pow(differenceY, 2)));

			robot.setAutoDelay(2);

			for (double current = 0.0; current < 1.0; current += step) {
				final int stepX = (int) (origin.getX() + (differenceX * current));
				final int stepY = (int) (origin.getY() + (differenceY * current));
				LOGGER.debug("Step to (" + stepX + ", " + stepY + ")");
				robot.mouseMove(stepX, stepY);
			}

			// just make sure we actually are at the right spot
			robot.setAutoDelay(450 + (int) (Math.random() * 100));
			robot.mouseMove(x, y);
			robot.setAutoDelay(0);
		}
	}

	public static enum MouseActions {
		NONE, LEFT_DOWN, LEFT_UP, RIGHT_DOWN, RIGHT_UP, MIDDLE_DOWN, MIDDLE_UP, LEFT_CLICK, RIGHT_CLICK, MIDDLE_CLICK,
		KEY_B, KEY_W, KEY_S, KEY_X;

		@Override
		public String toString() {

			switch (this) {
				case LEFT_CLICK:
					return "Left Button Click";
				case LEFT_DOWN:
					return "Left Button Down";
				case LEFT_UP:
					return "Left Button Up";
				case MIDDLE_CLICK:
					return "Middle Button Click";
				case MIDDLE_DOWN:
					return "Middle Button Down";
				case MIDDLE_UP:
					return "Middle Button Up";
				case NONE:
					return "No Action";
				case RIGHT_CLICK:
					return "Right Button Click";
				case RIGHT_DOWN:
					return "Right Button Down";
				case RIGHT_UP:
					return "Right Button Up";
				case KEY_B:
					return "Press B key";
				case KEY_S:
					return "Press S key";
				case KEY_W:
					return "Press W key";
				case KEY_X:
					return "Press X key";
				default:
					return "";
			}

		};
	}
}
