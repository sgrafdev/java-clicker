package net.suncrescent.clicker.robot.action;

public class Condition {

	public static enum ConditionType {
		PREVIOUS_CONDITION_TRUE, PREVIOUS_CONDITION_FALSE, PIXEL_COLOR_MATCHES, PIXEL_COLOR_NOT_MATCHES
	}

	public static enum ConditionChainOperator {
		OR, AND, XOR, EQUAL, NOT_EQUAL;
	}

	private ConditionType conditionType;
	private Object[] parameters = null;
	private Condition chainedCondition = null;
	private ConditionChainOperator chainedOperator = null;

	public Condition(ConditionType conditionType) {
		this.conditionType = conditionType;
	}

	public void setConditionType(ConditionType conditionType) {
		this.conditionType = conditionType;
	}

	public void setConditionParameters(Object... parameters) {
		this.parameters = parameters;
	}

	public Object[] getConditionParameters() {
		return this.parameters;
	}

	public ConditionType getConditionType() {
		return this.conditionType;
	}

	public void setChainedCondition(Condition condition, ConditionChainOperator operator) {
		this.chainedCondition = condition;
		this.chainedOperator = operator;
	}

	public Condition getChainedCondition() {
		return this.chainedCondition;
	}

	public ConditionChainOperator getChainedOperator() {
		return chainedOperator;
	}
}
