package net.suncrescent.clicker.robot.action;

import java.awt.Robot;

public class NullAction extends Action {

  public NullAction() {
  }

  public NullAction(int pause) {
    super(pause);
  }

  public NullAction(int pause, Condition condition) {
    super(pause, condition);
  }

  @Override
  public void executeAction(Robot robot) {
    // the null action does nothing...
  }
}
