package net.suncrescent.clicker.robot.action;

import java.util.ArrayList;

public interface ActionSource {
	public ArrayList<Action> getActionList();
}
