Little bot I made to cheat at Facebook games like Farmville.
It should work for any game where you can "collect" gold or money every X minutes.

1. Build a custom action script to click and collect money.
2. Leave the facebook game open while being AFK and let the bot farm your money.

The bot can somewhat "randomize" clicks so as to not appear to be a bot.

You can script decisision based on input such as pixel colors, useful
for games that have popup dialogs or other "anti-bot" measures.

To build your action script its helpful to have the bot run to
get x,y coordinates of things you want to click.
